<?php

namespace Api;

use Respect\Relational\Mapper;
use Respect\Rest\Router;

class Application
{
    protected $mapper;
    protected $router;
    protected static $input;

    protected static $instance;

    private function __construct()
    {
        try {
            $this->mapper = $this->createMapper();
            $this->router = new Router();
        } catch (\Exception $e) {
            die('Erro inesperado');
        }
    }

    private function createMapper()
    {
        if (getenv('OPENSHIFT_MYSQL_DB_HOST')) {
            $dbname = 'app';
            $host = getenv('OPENSHIFT_MYSQL_DB_HOST');
            $user = getenv('OPENSHIFT_MYSQL_DB_USERNAME');
            $password = getenv('OPENSHIFT_MYSQL_DB_PASSWORD');

            $db = new \PDO("mysql:host={$host};dbname={$dbname};charset=utf8;", $user, $password);
        } else {
            $db = new \PDO("mysql:host=localhost;dbname=app;charset=utf8;", 'root', '');
        }

        return (new Mapper($db));
    }

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Application();
        }

        return self::$instance;
    }

    public function mapper()
    {
        return $this->mapper;
    }

    public function router()
    {
        return $this->router;
    }

    public static function input()
    {
        if (!self::$input) {
            $input = array();
            $content = file_get_contents('php://input');
            $json = json_decode($content, true);

            if ($json) {
                $input = array_merge($json);
            }

            self::$input = (object) array_merge($input, $_REQUEST);
        }

        return self::$input;
    }
}