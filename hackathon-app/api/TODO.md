# Coisas a fazer:

Rotas
-------------------

## Profissional

- /api/profissional [GET] # obter todos, aceita filtros
- /api/profissional [PUT] # adicionar novo

- /api/profissional/{id} [GET] # obter um
- /api/profissional/{id} [PATCH] # atualizar um
- /api/profissional/{id} [DELETE] # apagar um

## Serviços prestados por um profissional

- /api/profissional/{id}/servicos [GET] # obtém todos, aceita filtros
- /api/profissional/{id}/servicos [PUT] # adiciona um serviço ao profissional
- /api/profissional/{id}/servicos/{id} [DELETE] # remove um serviço do profissional


## Salão

- /api/salao [GET] # obter todos, aceita filtros
- /api/salao [PUT] # adicionar novo

- /api/salao/{id} [GET] # obter um
- /api/salao/{id} [PATCH] # atualizar um
- /api/salao/{id} [DELETE] # apagar um

## Serviços prestados por um salão

- /api/salao/{id}/servicos [GET] # obtém todos, aceita filtros
- /api/salao/{id}/servicos [PUT] # adiciona um serviço ao profissional
- /api/salao/{id}/servicos/{id} [DELETE] # remove um serviço do salão

## Profissionais vinculados ao salão

- /api/salao/{id}/profissionais [GET] # exibir profissionais vinculados, aceita filtros
- /api/salao/{id}/profissionais [PUT] # vincular profissional
- /api/salao/{id}/profissionais/{id} [DELETE] # desvincular profissional

## Salões de um profissional

- /api/profissional/{id}/saloes [GET] # exibe salões em que profissional atua

## Cliente

- /api/cliente [GET] # obter todos, aceita filtros
- /api/cliente [PUT] # adicionar novo

- /api/cliente/{id} [GET] # obter um
- /api/cliente/{id} [PATCH] # atualizar um
- /api/cliente/{id} [DELETE] # apagar um


-------------------
# Prioridades:
-------------------

# Cliente

1. Obter por

# Serviços

2. Listar serviços

# 