<?php

$app = Api\Application::instance();

$router = $app->router();

$router->get('/api/salao/*', 'Api\Controller\Salao');
$router->patch('/api/salao/*', 'Api\Controller\Salao');

$router->get('/api/salao/*/profissionais', function($id) use ($app) {
    $mapper = $app->mapper();
    $filtros = $app->input();

    $pss = $mapper->profissional_salao(array(
        $filtroAprovado
    ))->salao[$id]->fetchAll();

    $profissionais = array();
    foreach ($pss as $profissional_salao) {
        if ($filtros->aprovado == 'true') {
            if ($profissional_salao->aprovado == '0') {
                continue;
            }
        }
        if ($filtros->aprovado == 'false') {
            if ($profissional_salao->aprovado == '1') {
                continue;
            }
        }

        $id = $profissional_salao->profissional_id;
        $profissionais[$id] = $mapper->profissional[$id]->fetch();
    }

    return array_values($profissionais);
});

$router->put('/api/salao/*/profissionais/*', function($id, $profissional) use ($app) {
    $mapper = $app->mapper();
    $dados = $app->input();

    $vinculo = new \stdClass();
    $vinculo->profissional_id = $profissional;
    $vinculo->salao_id = $id;
    $vinculo->aprovado = false;

    $mapper->profissional_salao->persist($vinculo);
    $mapper->flush();
});

$router->post('/api/salao/*/profissionais/*/aprovar', function($salao, $profissional) use ($app) {
    $mapper = $app->mapper();

    $vinculo = $mapper->profissional_salao(array(
        'profissional_id' => $profissional,
        'salao_id' => $salao
    ))->fetch();

    $vinculo->aprovado = true;
    $mapper->profissional_salao->persist($vinculo);
    $mapper->flush();
});

$router->delete('/api/salao/*/profissionais/*', function($salao, $profissional) use ($app) {
    $mapper = $app->mapper();

    $vinculo = $mapper->profissional_salao(array(
        'profissional_id' => $profissional,
        'salao_id' => $salao
    ))->fetch();

    $mapper->profissional_salao->remove($vinculo);
    $mapper->flush();
});