<?php

$app = Api\Application::instance();

$router = $app->router();

$router->get('/api/profissional/*/saloes', function($id) use ($app) {
    $mapper = $app->mapper();

    $saloes = $mapper->profissional_salao->profissional[$id]->fetchAll();

    $result = array();
    if ($saloes) {
        foreach ($saloes as $salao) {
            $result[] = $mapper->salao[$salao->salao_id]->fetch();
        }
    }

    return $result;
});
$router->get('/api/profissional/*', 'Api\Controller\Profissional');
$router->patch('/api/profissional/*', 'Api\Controller\Profissional');
