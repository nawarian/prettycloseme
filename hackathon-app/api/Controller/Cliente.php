<?php

namespace Api\Controller;

use Respect\Rest\Routable;

class Cliente implements Routable
{
    // /api/cliente/{id}
    // $id é opcional
    public function get($id = null)
    {
        return is_null($id) ? $this->fetchAll() : $this->fetch($id);
    }

    public function fetchAll()
    {
        $mapper = \Api\Application::instance()->mapper();
        return $mapper->cliente->fetchAll();
    }

    public function fetch($id)
    {
        $mapper = \Api\Application::instance()->mapper();
        $cliente = $mapper->cliente[$id]->fetch();
        
        if (!$cliente) {
            http_response_code(404);
        }

        return $cliente;
    }

    public function patch($id)
    {
        $cliente = $this->fetch($id);
        if (!$cliente) {
            http_response_code(404);
            return null;
        }

        $dados = \Api\Application::instance()->input();
        foreach (get_object_vars($cliente) as $chave => $valor) {
            if (property_exists($dados, $chave)) {
                $cliente->{$chave} = $dados->{$chave};
            }
        }

        $mapper = \Api\Application::instance()->mapper();
        try {
            $mapper->cliente->persist($cliente);
            $mapper->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 500);
        }
    }
}