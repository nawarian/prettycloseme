<?php
require_once dirname(__FILE__).'/../vendor/autoload.php';
require_once dirname(__FILE__).'/routes.php';

$router = Api\Application::instance()->router();
$router->always('Accept', array(
    'application/json' => 'json_encode'
));

print $router->run();