<?php

$app = Api\Application::instance();

$router = $app->router();

$router->get('/api/cliente/*', 'Api\Controller\Cliente');
$router->patch('/api/cliente/*', 'Api\Controller\Cliente');