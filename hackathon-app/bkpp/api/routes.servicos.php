<?php

$app = Api\Application::instance();

$router = $app->router();

$router->get('/api/servicos/*', 'Api\Controller\Servico');
$router->patch('/api/servicos/*', 'Api\Controller\Servico');