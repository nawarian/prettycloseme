<?php

$app = Api\Application::instance();

$router = $app->router();
$router->get('/api', function() {
    return array(
        'hallo' => 'welt'
    );
});

// Resource: Profissionais
include_once dirname(__FILE__).'/routes.profissional.php';
include_once dirname(__FILE__).'/routes.cliente.php';
include_once dirname(__FILE__).'/routes.salao.php';
include_once dirname(__FILE__).'/routes.servicos.php';
include_once dirname(__FILE__).'/routes.localizacao.php';
include_once dirname(__FILE__).'/routes.agendar.php';

$router->exceptionRoute('Exception', function (Exception $e) {
    http_response_code($e->getCode());
});