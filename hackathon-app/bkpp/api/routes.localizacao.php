<?php

$app = Api\Application::instance();

$router = $app->router();

$router->get('/api/profissionais/em/*/*', function($lat, $long) use ($app) {
    $mapper = $app->mapper();
    $dados = $app->input();

    $raio = $dados->raio ? $dados->raio : '5';

    $servicos = $dados->servicos ? $dados->servicos : null;
    if ($servicos) {
        $servicos = explode(',', $servicos);
    }

    $filtro = array();
    if (count($servicos)) {
        $filtro[] = 'servicos_id in ('. implode(',', $servicos). ')';
    }

    // Fazer calculo do range de latitude e longitude
    $filtroProfissional = array();

    $profissionais = $mapper->profissional_servicos($filtro)->profissional($filtroProfissional)->fetchAll();
    
    $result = array();
    foreach ($profissionais as $profissional) {
        $result[$profissional->id] = $profissional->profissional_id;
    }
    
    return array_values($result);
});