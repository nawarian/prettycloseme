<?php

namespace Api\Controller;

use Respect\Rest\Routable;

class Agendamento implements Routable
{
    // Agendar
    // {profissional: id, servico: id, cliente: id, salao: id|null}
    public function post()
    {
        $dados = \Api\Application::instance()->input();
        $mapper = \Api\Application::instance()->mapper();

        $profissional = $mapper->profissional[$dados->profissional]->fetch();
        $servico = $mapper->servicos[$dados->servico]->fetch();
        $cliente = $mapper->cliente[$cliente]->fetch();

        if (!property_exists($dados, 'salao')) {
            return $this->agendarDomicilio($profissional, $cliente, $servico);
        }

        $salao = $mapper->salao[$dados->salao]->fetch();
        return $this->agendarSalao($profissional, $cliente, $salao, $servico);
    }

    public function agendarDomicilio($profissional, $cliente, $servico)
    {
        $dados = \Api\Application::instance()->input();
        $mapper = \Api\Application::instance()->mapper();

        $de = \DateTime::createFromFormat('Y-m-d H:i:s', $dados->inicio);
        $ate = \DateTime::createFromFormat('Y-m-d H:i:s', $dados->final);

        if ($this->profissionalOcupado($profissional, $de, $ate)) {
            throw new \Exception(
                'Profissional ocupado.',
                304
            );
        }

        return $this->criarAgendamento($de, $ate, $profissional, $cliente, $servico);
    }

    private function criarAgendamento(
        \DateTime $de,
        \DateTime $ate,
        $profissional,
        $cliente,
        $servico,
        $salao = null
    ) {
        $mapper = \Api\Application::instance()->mapper();

        $agendamento = new \stdClass();
        $agendamento->inicial = $de->format('Y-m-d H:i:s');
        $agendamento->final = $ate->format('Y-m-d H:i:s');
        $agendamento->salao_id = !is_null($salao) ? $salao->id : null;
        $agendamento->profissional_id = $profissional->id;
        $agendamento->servico_id = $servico->id;

        $mapper->agendamento->persist($agendamento);
        $mapper->flush();
    }

    public function agendarSalao($profissional, $cliente, $salao, $servico)
    {}

    public function profissionalOcupado($profissional, \DateTime $de, \DateTime $ate)
    {
        $mapper = \Api\Application::instance()->mapper();

        $inicio = $de->format('Y-m-d H:i:s');
        $final = $ate->format('Y-m-d H:i:s');

        $agendamentos = $mapper->agendamento(array(
            'profissional_id' => $profissional->id,
            "inicial BETWEEN '$inicio' AND '$final' OR final BETWEEN '$inicio' AND '$final'"
        ))->fetchAll();

        return (bool) count($agendamentos);
    }
}