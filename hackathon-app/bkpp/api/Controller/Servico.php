<?php

namespace Api\Controller;

use Respect\Rest\Routable;

class Servico implements Routable
{
    // /api/servicos/{id}
    // $id é opcional
    public function get($id = null)
    {
        return is_null($id) ? $this->fetchAll() : $this->fetch($id);
    }

    public function fetchAll()
    {
        $mapper = \Api\Application::instance()->mapper();
        return $mapper->servicos->fetchAll();
    }

    public function fetch($id)
    {
        $mapper = \Api\Application::instance()->mapper();
        $servicos = $mapper->servicos[$id]->fetch();
        
        if (!$servicos) {
            http_response_code(404);
        }

        return $servicos;
    }

    public function patch($id)
    {
        $servicos = $this->fetch($id);
        if (!$servicos) {
            http_response_code(404);
            return null;
        }

        $dados = \Api\Application::instance()->input();
        foreach (get_object_vars($servicos) as $chave => $valor) {
            if (property_exists($dados, $chave)) {
                $servicos->{$chave} = $dados->{$chave};
            }
        }

        $mapper = \Api\Application::instance()->mapper();
        try {
            $mapper->servicos->persist($servicos);
            $mapper->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 500);
        }
    }
}