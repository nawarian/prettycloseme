<?php

namespace Api\Controller;

use Respect\Rest\Routable;

class Profissional implements Routable
{
    // /api/profissional/{id}
    // $id é opcional
    public function get($id = null)
    {
        return is_null($id) ? $this->fetchAll() : $this->fetch($id);
    }

    public function fetchAll()
    {
        $mapper = \Api\Application::instance()->mapper();
        return $mapper->profissional->fetchAll();
    }

    public function fetch($id)
    {
        $mapper = \Api\Application::instance()->mapper();
        $profissional = $mapper->profissional[$id]->fetch();
        
        if (!$profissional) {
            http_response_code(404);
        }

        return $profissional;
    }

    public function patch($id)
    {
        $profissional = $this->fetch($id);
        if (!$profissional) {
            http_response_code(404);
            return null;
        }

        $dados = \Api\Application::instance()->input();
        foreach (get_object_vars($profissional) as $chave => $valor) {
            if (property_exists($dados, $chave)) {
                $profissional->{$chave} = $dados->{$chave};
            }
        }

        $mapper = \Api\Application::instance()->mapper();
        try {
            $mapper->profissional->persist($profissional);
            $mapper->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 500);
        }
    }
}