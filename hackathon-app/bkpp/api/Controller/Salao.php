<?php

namespace Api\Controller;

use Respect\Rest\Routable;

class Salao implements Routable
{
    // /api/salao/{id}
    // $id é opcional
    public function get($id = null)
    {
        return is_null($id) ? $this->fetchAll() : $this->fetch($id);
    }

    public function fetchAll()
    {
        $mapper = \Api\Application::instance()->mapper();
        return $mapper->salao->fetchAll();
    }

    public function fetch($id)
    {
        $mapper = \Api\Application::instance()->mapper();
        $salao = $mapper->salao[$id]->fetch();
        
        if (!$salao) {
            http_response_code(404);
        }

        return $salao;
    }

    public function patch($id)
    {
        $salao = $this->fetch($id);
        if (!$salao) {
            http_response_code(404);
            return null;
        }

        $dados = \Api\Application::instance()->input();
        foreach (get_object_vars($salao) as $chave => $valor) {
            if (property_exists($dados, $chave)) {
                $salao->{$chave} = $dados->{$chave};
            }
        }

        $mapper = \Api\Application::instance()->mapper();
        try {
            $mapper->salao->persist($salao);
            $mapper->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 500);
        }
    }
}