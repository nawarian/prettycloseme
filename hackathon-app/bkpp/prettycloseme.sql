drop table if exists agendamento;
drop table if exists atendimento;
drop table if exists cliente;
drop table if exists galeria;
drop table if exists profissional;
drop table if exists profissional_servicos;
drop table if exists salao;
drop table if exists salao_servicos;
drop table if exists servicos;
drop table if exists profissional_salao;

CREATE TABLE agendamento (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  inicial DATETIME NULL,
  final DATETIME NULL,
  salao_id INTEGER UNSIGNED NULL,
  profissional_id INTEGER UNSIGNED NULL,
  servico_id INTEGER UNSIGNED NULL,
  valor_total DOUBLE NULL,
  PRIMARY KEY(id)
);

CREATE TABLE atendimento (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  dia_semana INTEGER UNSIGNED ZEROFILL NULL,
  horario_inicial DATETIME NULL,
  horario_final DATETIME NULL,
  profissional_id INTEGER UNSIGNED NULL,
  salao_id INTEGER UNSIGNED ZEROFILL NULL,
  PRIMARY KEY(id)
);

CREATE TABLE cliente (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NULL,
  cartao_credito VARCHAR(20) NULL,
  foto VARCHAR(255) NULL,
  idade INTEGER UNSIGNED NULL,
  PRIMARY KEY(id)
);

CREATE TABLE galeria (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  foto VARCHAR(255) NULL,
  profissional_id INTEGER UNSIGNED NULL,
  salao_id INTEGER UNSIGNED NULL,
  PRIMARY KEY(id)
);

CREATE TABLE profissional (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(4000) NULL,
  nome VARCHAR(500) NULL,
  qualificacao VARCHAR(4000) NULL,
  sexo VARCHAR(5) NULL,
  profissional_servicos_id INTEGER UNSIGNED NULL,
  foto VARCHAR(200) NULL,
  endereco_local VARCHAR(1000) NULL,
  atende_domicilio BOOL NULL,
  atendimento_id INTEGER UNSIGNED NULL,
  pontualidade BOOL NULL,
  raio_atuacao INTEGER UNSIGNED NULL,
  portifolio VARCHAR(4000) NULL,
  PRIMARY KEY(id)
);

ALTER TABLE `profissional` 
ADD COLUMN `atente_latitude` VARCHAR(45) NULL AFTER `portifolio`,
ADD COLUMN `atende_longitude` VARCHAR(45) NULL AFTER `atente_latitude`;


CREATE TABLE profissional_servicos (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  servicos_id INTEGER UNSIGNED NULL,
  profissional_id INTEGER UNSIGNED NULL,
  valor DOUBLE NULL,
  PRIMARY KEY(id)
);

CREATE TABLE profissional_salao (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  profissional_id INTEGER NOT NULL,
  salao_id INTEGER NOT NULL,
  aprovado BOOL  NULL,
  PRIMARY KEY (id)
);


CREATE TABLE salao (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(4000) NOT NULL,
  valor_hora DOUBLE NULL,
  endereco VARCHAR(1000) NULL,
  foto VARCHAR(200) NULL,
  salao_servicos_id INTEGER UNSIGNED NULL,
  cnpj_cpf VARCHAR(20) NULL,
  wifi BOOL NULL,
  tv BOOL NULL,
  estacionamento BOOL NULL,
  ar_condicionado BOOL NULL,
  PRIMARY KEY(id)
);

INSERT INTO profissional_salao VALUES (null, 1, 1, false);

INSERT INTO salao VALUES
(
  null, 'Salão XPTO 1', 25.00,
  'Vila Mariana', 'http://www.decorfacil.com/wp-content/uploads/2015/03/imagem-424.jpg',
  null, '123123123123',
  true,
  true,
  false,
  true
);

INSERT INTO salao VALUES
(
  null, 'Salão da Praia', 29.20,
  'Vila Mariana', 'http://belezachic.com.br/wp-content/uploads/Fachada-Salao-da-Praia-baixa1.jpg',
  null, '123123123123',
  true,
  true,
  false,
  true
);

CREATE TABLE salao_servicos (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  salao_id INTEGER UNSIGNED NULL,
  servicos_id INTEGER UNSIGNED NULL,
  valor INTEGER UNSIGNED NULL,
  PRIMARY KEY(id)
);

CREATE TABLE servicos (
  id INTEGER UNSIGNED AUTO_INCREMENT NOT NULL,
  descricao VARCHAR(500) NULL,
  PRIMARY KEY(id)
);

INSERT INTO cliente VALUES (
  null,
  'Níckolas Daniel',
  '919192139129',
  'https://avatars2.githubusercontent.com/u/3905582?v=3&s=460',
  '20'
);

INSERT INTO servicos (descricao) VALUES
('Corte de Cabelo'), ('Maqueagem simples'), ('Preparação para casamento');

INSERT INTO profissional
(descricao, nome, qualificacao, sexo, foto,
endereco_local, atende_domicilio, pontualidade,
raio_atuacao)
VALUES
(
  'Profissional 01 - descrição',
  'Profissional 01',
  'blablabla - qualificação',
  'F',
  'http://i44.photobucket.com/albums/f30/isaidpissoff/nicole068.jpg',
  'Vila Mariana',
  true,
  true,
  5
),
(
  'Profissional 02 - descrição',
  'Profissional 02',
  'blablabla - qualificação',
  'M',
  'http://i.telegraph.co.uk/multimedia/archive/01829/Mark-Woolley_1829782b.jpg',
  'Avenida Paulista',
  true,
  true,
  5
);

INSERT INTO profissional_servicos VALUES (null, 1, 1, 0), (null, 2, 1, 0), (null, 1, 2, 0), (null, 2, 2, 0), (null, 3, 2, 0);

UPDATE `app`.`profissional` SET `atente_latitude`='-23.5905222', `atende_longitude`='-46.638426' WHERE `id`='1';
UPDATE `app`.`profissional` SET `atente_latitude`='-23.5629203', `atende_longitude`='-46.6548236' WHERE `id`='2';

UPDATE `app`.`profissional` SET `descricao`='Autônoma , 27 anos com 5 de experiência, trabalha com maquiagem, manicure e escova', `nome`='Giovana Fernandes' WHERE `id`='1';
UPDATE `app`.`profissional` SET `descricao`='Autônoma , 31 anos com 11 de esperiência, trabalha com corte de cabelo , massagem e depilação.', `nome`='Jessica Marques', `sexo`='F' WHERE `id`='2';

UPDATE `app`.`salao` SET `descricao`='Beauty and Beauty - Salão com 10 anos de mercado, oferece um ambiente aconchegante e com infraestrutura\n\ncompleta, como: Ar, wifi, acesso a animais , tv a cabo e fácil acesso para deficientes.' WHERE `id`='1';
UPDATE `app`.`salao` SET `descricao`='OneStyle - Salão com 4 anos de mercado, otima localização com wifi.' WHERE `id`='2';
