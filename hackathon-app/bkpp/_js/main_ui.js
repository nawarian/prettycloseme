$(document).ready(function() {
    $('#toggle-menu').click(function() {
        $('nav').slideToggle();
    });
});


$(function() {
    var $document   = $(document),
        $inputRange = $('input[type="range"]');
    
    // Example functionality to demonstrate a value feedback
    function valueOutput(element) {
        var value = element.value,
            output = element.parentNode.getElementsByTagName('output')[0];
        output.innerHTML = value;
        $('#raio_d').css("background-size",(20+(value*10))+"%");
    }
    for (var i = $inputRange.length - 1; i >= 0; i--) {
        valueOutput($inputRange[i]);
    };
    $document.on('input', 'input[type="range"]', function(e) {
        valueOutput(e.target);
    });
    // end
    
    $('.logo-splash').click(function() {
        window.location.assign('/');
    });
  
    if ($inputRange && $inputRange.rangeslider) {
        $inputRange.rangeslider({
          polyfill: false 
        });
    }

});